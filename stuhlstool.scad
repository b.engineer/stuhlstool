include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
use <nutsnbolts/cyl_head_bolt.scad>



//sparse_strut3d(h=30, w=30, l=100, strut=2, maxang=50, max_bridge=20);
//sparse_strut(h=40, l=100, thick=20);


x=100;
y=30;
z=130;

num=y/2;
sc=0.07;
ro=4;
mo=1.4;


difference(){
cube([x,y,z], center=true);

ymove(y) for (i =[1:num]){
	scale([1 - i*sc,1,1 - i*sc]) ymove(-i*mo) yrot(i*ro) cuboid([x,y,z], chamfer=5);
};

ymove(-y) for (i =[1:num]){
	scale([1 - i*sc,1,1 - i*sc]) ymove(i*mo) yrot(i*ro) cuboid([x,y,z], chamfer=5);
};

xmove(x) for (i =[1:num]){
	scale([1,1 - i*sc,1 - i*sc]) xmove(-i) xrot(-i*ro*0.2) cuboid([x,y,z], chamfer=5);
};

xmove(x/3) up(z/2)hole_through(name="M8", l=20, cld=0.1, $fn=30);
xmove(x/3) up(z/2- 4)zrot(90)nutcatch_sidecut("M8", clh=0.1);
xmove(-x/3) up(z/2)hole_through(name="M8", l=20, cld=0.1, $fn=30);
xmove(-x/3) up(z/2- 4)zrot(90)nutcatch_sidecut("M8", clh=0.1);
}
